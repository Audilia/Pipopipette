$(document).ready(function () {
    $('.game-rules').click(function () {
        $('.rules').fadeToggle(1000);
    });
});

$(document).ready(function () {
    $('.game-rules2').click(function () {
        $('.rules2').fadeToggle(1000);
    });
});

$(document).ready(function () {
    $('.a').click(function () {
        var nomJoueur1 = $('#Utilisateur1').val();
        var nomJoueur2 = $('#Utilisateur2').val();
        $('.accueil').css({
            'display': 'none',
        });
        $('.j1').html(nomJoueur1);
        $('.j2').html(nomJoueur2);
    });
});

$(document).ready(function () {
    $('#carré').click(function () {
        $(this).css({
            'border-top-color': 'blue',
            'border-left-color': 'yellow',
            'border-bottom-color': 'red',
            'opacity': '1',
        });
    });
});

$(document).ready(function () {
    $('.clear').click(function () {
        $('#carré').css({
            'opacity': '0.2',
            'border-top-color': 'white',
            'border-left-color': 'white',
            'border-bottom-color': 'white',
        });
    });
});

